<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
	return response()->json([
		'status' => 200,
		'message' => 'Welcome'
	]); 
});

Route::get('login', function () {
    //return view('welcome');
	return response()->json([
		'status' => 401,
		'message' => 'Unauthorized'
	]); 
});

//Route::get('/list', 'ResourceController@listdata')->middleware('auth:api');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
