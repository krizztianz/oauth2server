# Oauth2 Authorization Server

Independent Oauth2 Authorization Server using Laravel Passport

**Installation**

Go ahead and try:

Clone the repo

```
$ git clone https://krizztianz@bitbucket.org/krizztianz/oauth2server.git
```

**Install the dependencies**

```
$ composer install
```

**Migrate the database**

```
$ php artisan migrate
```

Have fun!