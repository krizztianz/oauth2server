<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Resource extends Controller
{
    //
    public function listdata() {
        $data = array(
            array(
                "nama" => "Hasan",
                "alamat" => "Bekasi",
            ),
            array(
                "nama" => "Aziz",
                "alamat" => "Sawangan",
            ),
            array(
                "nama" => "Heri",
                "alamat" => "Citayam",
            ),
            array(
                "nama" => "Reza",
                "alamat" => "Pulogadung",
            ),
            array(
                "nama" => "Nofi",
                "alamat" => "Tebet",
            ),
        );
        
        return response()->json($data);
    }
}
